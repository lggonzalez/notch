#1) cargar las librerias
library("affy")
library("limma")
library("genefilter")

#2) importar targets
  #para las celulas KOPT-K1
targetsK <- readTargets("targetsK.txt", row.names="FileName")
  #para las celulas HPB-ALL
targetsH <- readTargets("targetsH.txt", row.names="FileName")

#3) importar data
  #para las celulas KOPT-K1
dataK <- ReadAffy(filenames=targetsK$FileName) 
  #para las celulas HPB-ALL
dataH <- ReadAffy(filenames=targetsH$FileName)

#4) Normalizar con RMA 
  #para celulas KOPT-K1
esetK <- expresso(dataK,
                 bg.correct = TRUE, 
                 bgcorrect.method="rma",
                 normalize = TRUE, 
                 normalize.method="quantiles", 
                 pmcorrect.method="pmonly", 
                 summary.method="medianpolish",
                 verbose = TRUE
) 
  #para celulas HPB-ALL
esetH <- expresso(dataH,
                 bg.correct = TRUE, 
                 bgcorrect.method="rma",
                 normalize = TRUE, 
                 normalize.method="quantiles", 
                 pmcorrect.method="pmonly", 
                 summary.method="medianpolish",
                 verbose = TRUE
) 

#5) BOXPLOTS antes y despues de la normalización
# Boxplot para raw data
  #para celulas KOPT-K1
boxplot(dataK,
        main="Boxplot Before Normalization para dataK",
        col = "cyan")
  #para celulas HPB-ALL
boxplot(dataH,
        main="Boxplot Before Normalization para dataH",
        col = "cyan")

#Boxplot para normalized data
  #para celulas KOPT-K1
exprsesetK <- as.data.frame(exprs(esetK))		
boxplot(data.frame(exprsesetK),
        main="Boxplot After Normalization (log scale) para dataK",
        col = "aquamarine")
  #para celulas HPB-ALL
exprsesetH <- as.data.frame(exprs(esetH))		
boxplot(data.frame(exprsesetH),
        main="Boxplot After Normalization (log scale) para dataH",
        col = "aquamarine")

#6) Filtrar data utilizandi IQR.
  #para celulas KOPT-K1
esetIQRK <- varFilter(esetK, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
  #para celulas HPB-ALL
esetIQRH <- varFilter(esetH, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)




          #Analisis de Expresion diferencial#


design<-cbind(KOP.Control=c(1,1,1,0,0,0,0,0,0,0,0,0),
              HPB.Control=c(0,0,0,1,1,1,0,0,0,0,0,0), 
              KOP.Prueba=c(0,0,0,0,0,0,1,1,1,0,0,0),
              HPB.Prueba=c(0,0,0,0,0,0,0,0,0,1,1,1))
rownames(design)<-targets$FileName

design<-cbind(Control=c(1,1,1,0,0,0),
              Prueba=c(0,0,0,1,1,1)) 


cont.matrix<-makeContrasts(ControlvsPrueba=Control-Prueba,
                           HPB.ControlvsHPB.Prueba=HPB.Control-HPB.Prueba,
                           levels=design)

#7) Diseñar Matíz Design matriz
  #para celulas KOPT-K1
designK<-cbind(Control=c(1,1,1,0,0,0), prueba=c(0,0,0,1,1,1))
rownames(designK)<-targetsK$FileName
  #para celulas HPB-ALL
designH<-cbind(Control=c(1,1,1,0,0,0), prueba=c(0,0,0,1,1,1))
rownames(designH)<-targetsH$FileName


#8) Matriz de contraste
  #para celulas KOPT-K1
cont.matrixK<-makeContrasts(pruebavsControl=prueba-Control,levels=designK) 
  #para celulas HPB-ALL
cont.matrixH<-makeContrasts(pruebavsControl=prueba-Control,levels=designH) 

#9. Obtener differentially expressed genes (DEGs)
  #Linear model and eBayes para celulas KOPT-K1
fitK<-lmFit(esetIQRK,designK)  ##getting DEGs from IQR 
fit2K<-contrasts.fit(fitK, cont.matrixK)
fit2K<-eBayes(fit2K)
toptableIQRK<-topTable(fit2K, number=dim(exprs(esetIQRK))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRK,file="ResultadosK.RData")

  #Linear model and eBayes para celulas HPB-ALL
fitH<-lmFit(esetIQRH,designH)  ##getting DEGs from IQR 
fit2H<-contrasts.fit(fitH, cont.matrixH)
fit2H<-eBayes(fit2H)
toptableIQRH<-topTable(fit2H, number=dim(exprs(esetIQRH))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRH,file="ResultadosH.RData")
