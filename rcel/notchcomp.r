#1) cargar las librerias
library("affy")
library("limma")
library("genefilter")
library("GEOquery")
library("pheatmap")

#2) importar targets
#para las celulas KOPT-K1
targetsK <- readTargets("targetsK.txt", row.names="FileName")
#para las celulas HPB-ALL
targetsH <- readTargets("targetsH.txt", row.names="FileName")
#importar targets para ambas lineas celulares
targets <- readTargets("targets.txt", row.names="FileName")


#3) importar data
#para las celulas KOPT-K1
dataK <- ReadAffy(filenames=targetsK$FileName) 
#para las celulas HPB-ALL
dataH <- ReadAffy(filenames=targetsH$FileName)
#para ambas lineas celulares
data <- ReadAffy(filenames=targets$FileName) 

phenoData(data)
head(pData(data))

#Hacer combinacion del strain y tratamiento en un solo vector
TS <- paste(targets$Strain, targets$Treatment, sep=".")
TS <- factor(TS, levels=c("KOP.Control","KOP.Prueba","HPB.Control","HPB.Prueba"))
S <- targets$Strain
T <- targets$Treatment

#4) Normalizar con RMA 
#para celulas KOPT-K1
esetK <- expresso(dataK,
                  bg.correct = TRUE, 
                  bgcorrect.method="rma",
                  normalize = TRUE, 
                  normalize.method="quantiles", 
                  pmcorrect.method="pmonly", 
                  summary.method="medianpolish",
                  verbose = TRUE
) 
#para celulas HPB-ALL
esetH <- expresso(dataH,
                  bg.correct = TRUE, 
                  bgcorrect.method="rma",
                  normalize = TRUE, 
                  normalize.method="quantiles", 
                  pmcorrect.method="pmonly", 
                  summary.method="medianpolish",
                  verbose = TRUE
) 
#para ambas lineas celulares
eset <- expresso(data,
                 bg.correct = TRUE, 
                 bgcorrect.method="rma",
                 normalize = TRUE, 
                 normalize.method="quantiles", 
                 pmcorrect.method="pmonly", 
                 summary.method="medianpolish",
                 verbose = TRUE
)

#5) Filtrar data utilizandi IQR.
#para celulas KOPT-K1
esetIQRK <- varFilter(esetK, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQRK) <- row.names(targetsK)
head(exprs(esetIQRK))
#para celulas HPB-ALL
esetIQRH <- varFilter(esetH, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQRH) <- row.names(targetsH)
head(exprs(esetIQRH))
#para ambas lineas celulares
esetIQR <- varFilter(eset, var.func=IQR, var.cutoff=0.5, filterByQuantile=TRUE)
colnames(esetIQR) <- row.names(targets)
head(exprs(esetIQR))

#6) Diseñar Matríz 
#para celulas KOPT-K1
designK<-cbind(Control=c(1,1,1,0,0,0), prueba=c(0,0,0,1,1,1))
rownames(designK)<-targetsK$FileName
#para celulas HPB-ALL
designH<-cbind(Control=c(1,1,1,0,0,0), prueba=c(0,0,0,1,1,1))
rownames(designH)<-targetsH$FileName
#para ambas lineas celulares
design<-cbind(KOP.Control=c(1,1,1,0,0,0,0,0,0,0,0,0),
              HPB.Control=c(0,0,0,1,1,1,0,0,0,0,0,0), 
              KOP.Prueba=c(0,0,0,0,0,0,1,1,1,0,0,0),
              HPB.Prueba=c(0,0,0,0,0,0,0,0,0,1,1,1))
rownames(design)<-targets$FileName


#design <- model.matrix(~0+TS)  #otra forma de diseñar la matriz que encontre en limma
#rownames(design)<-targets$FileName
#colnames(design) <- levels(TS)



#7) Matriz de contraste
#para celulas KOPT-K1
cont.matrixK<-makeContrasts(pruebavsControl=prueba-Control,levels=designK) 
#para celulas HPB-ALL
cont.matrixH<-makeContrasts(pruebavsControl=prueba-Control,levels=designH) 
#para ambas lineas celulares
#cont.matrix <- makeContrasts(KOP.ControlvsKOP.Prueba=KOP.Control-KOP.Prueba,HPB.ControlvsHPB.Prueba=HPB.Control-HPB.Prueba,levels=design)
#cont.matrix <- makeContrasts(ControlvsPruebainKOP=KOP.Control-KOP.Prueba, ControlvsPruebainHPB=HPB.Control-HPB.Prueba, Diff=(KOP.Control-KOP.Prueba)-(HPB.Control-HPB.Prueba), levels=design)
cont.matrix <- makeContrasts(ControlvsPruebainKOP=KOP.Control-KOP.Prueba, ControlvsPruebainHPB=HPB.Control-HPB.Prueba, 
                             Diff=(KOP.Control-KOP.Prueba)-(HPB.Control-HPB.Prueba), levels=design)
#8. Obtener differentially expressed genes (DEGs)
#Linear model and eBayes para celulas KOPT-K1
fitK<-lmFit(esetIQRK,designK)  ##getting DEGs from IQR 
fit2K<-contrasts.fit(fitK, cont.matrixK)
fit2K<-eBayes(fit2K)
toptableIQRK<-topTable(fit2K, number=dim(exprs(esetIQRK))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRK,file="ResultadosK.RData")

#Linear model and eBayes para celulas HPB-ALL
fitH<-lmFit(esetIQRH,designH)  ##getting DEGs from IQR 
fit2H<-contrasts.fit(fitH, cont.matrixH)
fit2H<-eBayes(fit2H)
toptableIQRH<-topTable(fit2H, number=dim(exprs(esetIQRH))[1], adjust.method="BH", sort.by="p") #crear tabla
save(toptableIQRH,file="ResultadosH.RData")

#para ambas lineas celulares
fit <- lmFit(eset, design)
fit<-eBayes(fit)
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2)
toptableIQR<-topTable(fit2, number=dim(exprs(esetIQR))[1], adjust.method="BH", sort.by="F")
save(toptableIQR,file="ResultadosGlobal.RData")


#differentially expressed cells
fit <- lmFit(y,design,block=targets$TS,correlation=dupcor$consensus.correlation)
contrasts <- makeContrasts(ML-MS, LP-MS, ML-LP, levels=design)
fit2 <- contrasts.fit(fit, contrasts)
fit2 <- eBayes(fit2, trend=TRUE)
summary(decideTests(fit2, method="global"))


#Ver resultados de ambas lineas celulares
plotMDS(eset)
results <- decideTests(fit2)
vennDiagram(results)


####Anotacion
BiocManager::install("hgu133plus2.db")
library(hgu133plus2.db)
hgu133plus2()

#Todas las lineas celulares
load("ResultadosGlobal.RData")
ID.fdr.005.table<-subset(toptableIQR, toptableIQR$adj.P.Val<=0.05)
probenames.fdr.005<-as.character(rownames(ID.fdr.005.table))
list.GeneSymbol.fdr.005<-mget(probenames.fdr.005, hgu133plus2SYMBOL)
char.GeneSymbol.fdr.005<- as.character(list.GeneSymbol.fdr.005)
toptable.annotated<-cbind(ID.fdr.005.table,char.GeneSymbol.fdr.005)

#library("annotate")
#GeneSymbol2.fdr.005<-getSYMBOL(probenames.fdr.005,"hgu133plus2.db")	

#celulas H
load("ResultadosH.RData")
ID.H.table<-subset(toptableIQRH, toptableIQRH$adj.P.Val<=0.05)
probenames.H<-as.character(rownames(ID.H.table))
list.GeneSymbol.H<-mget(probenames.H, hgu133plus2SYMBOL)
char.GeneSymbol.H<- as.character(list.GeneSymbol.H)
toptable.annotatedH<-cbind(ID.H.table,char.GeneSymbol.H)

#celulas K
load("ResultadosK.RData")
ID.K.table<-subset(toptableIQRK, toptableIQRK$adj.P.Val<=0.05)
probenames.K<-as.character(rownames(ID.K.table))
list.GeneSymbol.K<-mget(probenames.K, hgu133plus2SYMBOL)
char.GeneSymbol.K<- as.character(list.GeneSymbol.K)
toptable.annotatedK<-cbind(ID.K.table,char.GeneSymbol.K)


#Expression matrix
exprs(eset)
pData(eset)
#output.gtc(eset)
#output.cls()